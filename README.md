Multi-step Wizard
===================================

###Software:

- Drupal 7.56


###Deployment info :

- Install Site & DB (zemoga.sql)
- In case you don't want to install DB, follow the installation process and activate AJAX Register Wizard module on (admin/modules)
- Make sure you have Date, Date API & Date Popup module enable to activate AJAX Register Wizard module

###How to review the test:
- code is on (sites/all/modules/custom/zemoga_registration/)
- Please go to zemoga-user URL to get the wizard
- Data is stored on zemoga_user DB table.

###Contrib modules used:

- date
- date_popup